const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const bcrypt = require("bcryptjs");

app.set("port",8080);
app.use(bodyParser.json({type:"application/json:"}));
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());

const Pool = require("pg").Pool;
const config = {
		host: "localhost",
		user: "postgres",
		password: "Skcbd9814",
		database: "postgres"
};

const pool = new Pool(config);

app.post("/api/login", async(req,res) => {
		const u = req.body.username;
		const p = req.body.password;
		console.log(u);
		
		try{
				const test = "select * from users where name = $1";
				const response = await pool.query(test,[req.body.username]);
				if(response.rows == 0){
						res.json({status:"no user"});
				}else{
						if(await bcrypt.compare(p,response.rows[0].password)) {
								const username = response.rows[0].name;
								const zip = response.rows[0].zip;
								res.json({status:"succesful",username:username,zipcode:zip});
						}else{
								res.json({status:"compare fail"});
						}
				}
		}catch(e){
				res.json({status:"error"});
		}
});


app.post("/api/search", async(req,res) => {
		const s = req.body.s;
		const z = req.body.zip;
		console.log(z);
		console.log(s);

		try{
				if(z === '0'){
						if(s==="movies"){
								const response = await pool.query("select * from movies");
								const movies = response.rows.map(function(item) {
										return {
												movie:item.movie,
												theater:item.theater,
												address:item.address,
												city:item.city,
												zip:item.zip
										}
								});
								res.json({status:"movies",movies:movies});
						}else{
								const movies = "select * from movies where movie like $1";
								const r1 = await pool.query(movies,['%' + s + '%']);
								console.log(r1);
								if(r1.rows != 0){
										console.log(r1);
										const movie = r1.rows.map(function(item){
												return{
														movie:item.movie,
														theater:item.theater,
														address:item.address,
														city:item.city,
														zip:item.zip
												}
										});
										res.json({status:"movies",movies:movie});
								}else{
										const restaurant = "select stores.name as name,types.name as type,stores.address as address,stores.zip as zipcode,stores.city as city from stores inner join connection on stores.store_id = connection.store inner join types on types.type_id = connection.type where stores.name like $1";
										const r1 = await pool.query(restaurant,['%' + s + '%']);
										if(r1.rows != 0){
												const restaurants = r1.rows.map(function(item){
														return{
																name:item.name,
																type:item.type,
																address:item.address,
																zip:item.zipcode,
																city:item.city
														}
												});
												res.json({status:"restaurants",restaurants:restaurants});
										}else{
		const restaurant = "select stores.name as name,types.name as type,stores.address as address,stores.zip as zipcode,stores.city as city from stores inner join connection on stores.store_id = connection.store inner join types on types.type_id = connection.type where types.name like $1";
										const r1 = await pool.query(restaurant,['%' + s + '%']);
										if(r1.rows != 0){
												const restaurants = r1.rows.map(function(item){
														return{
																name:item.name,
																type:item.type,
																address:item.address,
																zip:item.zipcode,
																city:item.city
														}
												});
												res.json({status:"restaurants",restaurants:restaurants});

														
										}else{res.json({status:"empty"}); }
								}
								}
						}
				}else{
						if(s==="movies"){
								const template = "select * from movies where zip = $1";
								const response = await pool.query(template,[z]);
								const movies = response.rows.map(function(item) {
										return {
												movie:item.movie,
												theater:item.theater,
												address:item.address,
												city:item.city,
												zip:item.zip
										}
								});
								res.json({status:"movies",movies:movies});
						}else{
								const movies = "select * from movies where movie like $1 and zip = $2";
								const r1 = await pool.query(movies,['%' + s + '%',z]);
								console.log(r1);
								if(r1.rows != 0){
										console.log(r1);
										const movie = r1.rows.map(function(item){
												return{
														movie:item.movie,
														theater:item.theater,
														address:item.address,
														city:item.city,
														zip:item.zip
												}
										});
										res.json({status:"movies",movies:movie});
								}else{
										const restaurant = "select stores.name as name,types.name as type,stores.address as address,stores.zip as zipcode,stores.city as city from stores inner join connection on stores.store_id = connection.store inner join types on types.type_id = connection.type where stores.name like $1 and stores.zip = $2";
										const r1 = await pool.query(restaurant,['%' + s + '%',z]);
										if(r1.rows != 0){
												const restaurants = r1.rows.map(function(item){
														return{
																name:item.name,
																type:item.type,
																address:item.address,
																zip:item.zipcode,
																city:item.city
														}
												});
												res.json({status:"restaurants",restaurants:restaurants});
										}else{
		const restaurant = "select stores.name as name,types.name as type,stores.address as address,stores.zip as zipcode,stores.city as city from stores inner join connection on stores.store_id = connection.store inner join types on types.type_id = connection.type where types.name like $1 and stores.zip = $2";
										const r1 = await pool.query(restaurant,['%' + s + '%',z]);
										if(r1.rows != 0){
												const restaurants = r1.rows.map(function(item){
														return{
																name:item.name,
																type:item.type,
																address:item.address,
																zip:item.zipcode,
																city:item.city
														}
												});
												res.json({status:"restaurants",restaurants:restaurants});

														
										}else{res.json({status:"empty"});}
								}

								}
						}
				}
		}catch(e){
				res.json({status:"error"});
		}
});
app.post("/api/register", async(req,res) => {
		const u = req.body.username;
		const p = req.body.password;
		const z = req.body.zip;
		console.log(u);

		try{
				const t = "select * from users where name = $1";
				const test = await pool.query(t,[u]);
				if(test.rowCount == 0){
console.log(u);
						const salt = await bcrypt.genSalt();
						const hash = await bcrypt.hash(p,salt);
						const template = "insert into users (name,password,zip) values ($1,$2,$3)";
						const response = await pool.query(template,[u,hash,z]);
						res.json({status:"registered"});
				}else{
						res.json({status:"exists"});
				}
		}catch(e){
				res.json({status:"error"});
		}
});



								
app.listen(app.get("port"), () => {
		console.log(`Find the server at http://localhost:${app.get("port")}`);
});


