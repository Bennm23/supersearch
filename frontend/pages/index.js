import Header from '../components/Header.js';
import React from 'react';

class Index extends React.Component {
		constructor(props){
				super(props);
		}



		render() {
				return(
						<div>
						<Header />
							<div style = {{margin:"auto auto",textAlign:"center"}}>
								<h1>Welcome to your favorite search engine!</h1>
								<p> Feel free to search without logging in, but if you would like localized results, please register an account</p>
							</div>
						</div>
				)
		}
}
export default Index;

