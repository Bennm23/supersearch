import Header from '../components/Header.js';
import React from 'react';
import jsCookie from 'js-cookie';
import Link from 'next/link';

const PostLink = props => (
		<li>
			<Link href={`/post?title=${props.title}`}>
				<a>{props.title}</a>
			</Link>
		</li>
);

class Search extends React.Component {
		constructor(props) {
				super(props);
				this.state={search:"",result:[]};
		}

		handleUpdate(evt){
				this.setState({search:evt.target.value});
				this.handleSearch(evt.target.value);
		}

		async handleSearch(evt){
				const header = {
						"Accept":"application/json",
						"Content-Type":"application/x-www-form-urlencoded"
				};
				
				const z = jsCookie.get('zip');
				if(jsCookie.get('zip')){
					const searchParams = new URLSearchParams("?s="+this.state.search+"&zip="+z);
						const search = await fetch(`http://35.245.223.149/api/search`,{
						method:'POST',
						headers: header,
						body:searchParams
				});

				const results = await search.json();
				this.setState({result: results});

				}else{
				
				const searchParams = new URLSearchParams("?s="+this.state.search+"&zip="+0);
				const search = await fetch(`http://35.245.223.149/api/search`,{
						method:'POST',
						headers:header,
						body:searchParams
				});

				const results = await search.json();
				this.setState({result: results});
				}

		}





		render() {
				return(
						<div>
							<Header />
							<div style = {{margin:"auto auto", textAlign:"center"}}>
							<h1>Search</h1>
							<p><input type='text' value={this.state.search} onChange={this.handleUpdate.bind(this)}/></p>
									<table style={{width:"100%"}}>
						{"result" in this.state && this.state.result.status == "movies"&&this.state.search != "" ?
											<tr>
											<th>Movie</th>
											<th>Theater</th>
											<th>Address</th>
											<th>City</th>
											<th>ZipCode</th>
											</tr>
								: null
						}
						
						{"result" in this.state && this.state.result.status == "movies" && this.state.search != "" ?
										this.state.result.movies.map((item) => (
															<tr>
															<td>{item.movie}</td>
															<td>{item.theater}</td>
															<td>{item.address}</td>
															<td>{item.city}</td>
															<td>{item.zip}</td>
															</tr>
											))
										: null
						}
						{"result" in this.state && this.state.result.status == "restaurants"&&this.state.search != "" ?
											<tr>
											<th>Name</th>
											<th>Type</th>
											<th>Address</th>
											<th>City</th>
											<th>ZipCode</th>
											</tr>
								: null
						}
						
						{"result" in this.state && this.state.result.status == "restaurants" && this.state.search != "" ?
										this.state.result.restaurants.map((item) => (
															<tr>
															<td>{item.name}</td>
															<td>{item.type}</td>
															<td>{item.address}</td>
															<td>{item.city}</td>
															<td>{item.zip}</td>
															</tr>
											))
										: null
						}


									</table>
						
							</div>
						</div>
			);
		
		}
}

export default Search;
