import React from 'react';
import Router from 'next/router';
import Header from '../components/Header.js';
import Link from 'next/link';

class Register extends React.Component{
		constructor(props){
				super(props);
				this.state = {username:'',password:'',zipcode:''};
		}

		handleUsernameUpdate(evt){
			this.setState({username:evt.target.value});
		}
		handlePasswordUpdate(evt){
				this.setState({password:evt.target.value});
		}
		handleZipUpdate(evt){
				this.setState({zipcode:evt.target.value});
		}
		async handleRegister(evt){
				const header = {
						"Accept":"application/json",
						"Content-Type":"application/x-www-form-urlencoded"
				};
				const searchParams = new URLSearchParams("username="+this.state.username+"&password="+this.state.password+"&zip="+this.state.zipcode);

				const register = await fetch(`http://35.245.223.149/api/register`, {
						method: 'POST',
						headers: header,
						body: searchParams
				});
				const response = await register.json();
				console.log(response);
				if(response.status == "registered"){
						Router.replace("/login");
				}
		}

		render() {
				return(
						<div>
							<Header />
							<div style={{textAlign:'center',margin:'auto auto'}}>
								<h1>Register</h1>
								<p>Username<input type='text' value={this.state.username}
								onChange={this.handleUsernameUpdate.bind(this)}></input></p>
								<p>Password<input type='password' value={this.state.password}
								onChange={this.handlePasswordUpdate.bind(this)}></input></p>
								<p>Zipcode<input type='text' value={this.state.zipcode}
								onChange={this.handleZipUpdate.bind(this)}></input></p>
								<p><button onClick={this.handleRegister.bind(this)}>Register</button></p>
								<br />
								<p>
								<Link href="/login">
									<a>Already Registered?</a>
								</Link>
								</p>
							</div>
						</div>
				)
		}
}
export default Register;
