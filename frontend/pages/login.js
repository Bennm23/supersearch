import Header from '../components/Header.js';
import React from 'react';
import Link from 'next/link';
import jsCookie from 'js-cookie';
import Router from 'next/router';

const PostLink = props => (
		<li>
			<Link href={`post?title=${props.title}`}>
				<a>{props.title}</a>
			</Link>
		</li>
)

class Login extends React.Component {
		constructor(props){
				super(props);
				this.state = {
						username:"",
						password:""
				};
		}

		handleUsernameUpdate(evt){
				this.setState({username:evt.target.value});
		}
		handlePasswordUpdate(evt){
				this.setState({password:evt.target.value});
		}
		async handleLogin(evt){
				
				const header = {
						"Accept" : "application/json",
						"Content-Type": "application/x-www-form-urlencoded"
				};
				const searchParams = new URLSearchParams("username="+this.state.username+"&password="+this.state.password);
				console.log(this.state.username);
				const user = await fetch(`http://35.245.223.149/api/login`,{
						method:'POST',
						headers: header,
						body: searchParams
				});
				
				const users = await user.json();
				console.log(users);
				if(users.status =="succesful"){
						jsCookie.set('username',users.username);
						jsCookie.set('zip',users.zipcode);
						Router.replace("/search");
				}
	}



		render (){
			return(
				<div>
					<Header />
					<div style={{margin:"auto auto",textAlign:"center"}}>
						<h1>Login</h1>
						<p>Username <input type='text' value={this.state.username} 
						onChange={this.handleUsernameUpdate.bind(this)}/></p>
						<p>Password <input type='password' value={this.state.password} 
						onChange={this.handlePasswordUpdate.bind(this)}/></p>
						<p><button onClick={this.handleLogin.bind(this)}>Login</button></p>
						<br />
						<p>
							<Link href ="/register">
								<a>Register</a>
							</Link>
						</p>
					</div>
	
				</div>
			)
		}
}
export default Login;
