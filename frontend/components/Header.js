import Link from 'next/link';
import Router from 'next/router';
import jsCookie from 'js-cookie';

const linkStyle = {
  marginRight: 15
}

class Header extends React.Component {
		constructor(props){
				super(props);
				this.state = {username:''};
		}
logout(evt){
		jsCookie.remove('username');
		jsCookie.remove('zip');
		Router.replace("/login");
}


render(){
  return (
    <div>

		  {jsCookie.get('username') && 
				  	<a style = {{marginRight:15,color:"#000000",cursor:"default",pointerEvents:'none'}}>{jsCookie.get('username')}</a>
		  }
		  {jsCookie.get('username') && 
						  <Link href="/">
						  <a style={linkStyle} onClick={this.logout.bind(this)}>Logout</a>
		  					</Link>
		  }
		  {!jsCookie.get('username') &&
					<Link href="/login">
						  <a style={linkStyle}>Login</a>
					</Link>
		  }
      <Link href="/">
        <a style={linkStyle}>Home</a>
      </Link>
      <Link href="/search">
        <a style={linkStyle}>Search</a>
      </Link>
    </div>
  )
}
}
export default Header;
